<?php
require_once('config/app.php');
require_once('config/folder_image.php');
session_start();
if(!isset($_SESSION['success_login'])) {
	header('Location: http://demo.local/users/login.php');
	die();
}
$email_login = null;
if(isset($_SESSION['email_login'])) {
	$email_login = $_SESSION['email_login'];
}
$data = "SELECT * FROM users";
if($email_login != 'admin@admin.com') {
	$data .= " WHERE email = '$email_login'";
}
$result = $conn->query($data);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	
	<div class="container">
		<p style="color: red">
			<?php
			if(isset($_SESSION['success_edit_user'])) {
				echo $_SESSION['success_edit_user'];
				unset($_SESSION['success_edit_user']);
			}
			if(isset($_SESSION['success_add_user'])) {
				echo $_SESSION['success_add_user'];
				unset($_SESSION['success_add_user']);
			}
			if(isset($_SESSION['success_delete_user'])) {
				echo $_SESSION['success_delete_user'];
				unset($_SESSION['success_delete_user']);
			}
			if(isset($_SESSION['success_update_password'])) {
				echo $_SESSION['success_update_password'];
				unset($_SESSION['success_update_password']);
			}
			?>
		</p>
		<div>
			<a class="btn btn-primary" href="/users/add_user.php">Thêm user</a>
			<a class="btn btn-primary" href="/history.php">History</a>
			<a class="btn btn-primary" href="/images/images.php">Danh sách image</a>
		</div>
		<table class="table table-bordered table-hover">
			<tr>
				<th>stt</th>
				<th>avatar</th>
				<th>name</th>
				<th>email</th>
				<th>password</th>
				<th>edit</th>
			</tr>
			
			<?php while($row = $result->fetch_assoc()) { ?>
			<tr>
	            <td><?php echo $row['id']; ?></td>
	            <?php
	            $path_image = null;
	            $name_image = null;
	            $avatar_id = $row['avatar'];
	            $getimage = "SELECT * FROM images WHERE id = '$avatar_id'";
	            $check_query_image = $conn->query($getimage);
	            if($check_query_image->num_rows > 0) {
	            	$data_image = mysqli_fetch_object($check_query_image);
	            	if(array_key_exists('path_name', $data_image)) {
	            		$path_image = $data_image->path_name;
	            	}
	      			if(array_key_exists('name', $data_image)) {
	      				$name_image = $data_image->name;
	      			}
	            }
	            ?>
	            <td><img width="100" height="100" src="<?php echo FOLDER_IMAGE . $path_image . "/" . $name_image ?>"></td>
		        <td><?php echo $row['name']; ?></td>
		        <td><a href="/users/edit_users.php?id=<?php echo $row['id'] ?>"><?php echo $row['email']; ?></a></td>
		        <td><?php echo $row['password']; ?></td>
		        <td><a class="btn btn-primary" href="/users/edit_password.php?id=<?php echo $row['id'] ?>">Đổi password</a></td>
		    </tr>
			<?php } ?>

		</table>
		<div>
			<a class="btn btn-primary" href="/users/handle_users/handle_logout_user.php">Đăng xuất</a>
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			var a = 0;
			var b = 0;
			var d = 0;
			$(".number").click(function(){
				var content = $('.result').text() + $(this).attr("number");
				$('.result').html(content);
			});
			$(".clear").click(function(){
				$('.result').html("0");
			});
			$(".back").click(function(){
				var testt = '0123456789';
				var text = $('.result').text().slice(0,-1);				
				$('.result').html(text);
			});
			$(".calculate").click(function(){
				var check = $('.result').text();
				var checkcalculate = ["x", "+", "-", ":"];
				console.log('dwadwaagg',check);

				console.log('tptptptp',checkcalculate);

				var test = checkcalculate.some(key=>check.includes(key));
				console.log('ttttttpppppp',test);
				if(!test) {
					a = $('.result').text();
					var content = $('.result').text() + $(this).text();
					d = content;
				 	$('.result').html(content);
				}
			});
			$(".run").click(function(){
				var c = 0;
				b = $(".result").text().replace(d,'');
				var pheptinh = d.replace(a,''); // "+"

				var numbera = parseInt(a);
				var numberb = parseInt(b);
				if (pheptinh == "+") {
					var c = numbera + numberb;
				}
				if (pheptinh == "-") {
					var c = numbera - numberb;
				}
				
				console.log('aaaaaaaaaaaaaa',numbera);
				console.log('bbbbbbbbbbbbbb',numberb);
				$(".result").html(c);	
			})
		})
	</script>

</body>
</html>