<?php

class Database
{
	
	private $conn;
	private $servername;
	private $username;
	private $password;
	private $dbname;

    public function __construct()
    {
    	$this->servername = "localhost";
    	$this->username = "root";
    	$this->password = "";
    	$this->dbname = "demo";
		$this->connection();
    }

    public function getOne($query)
    {
        return $this->conn->query($query)->fetch_assoc();
    }

    public function getMany($query)
    {
        return $this->conn->query($query);
    }

    private function connection()
    {
    	$this->conn = new mysqli(
    		$this->servername, 
    		$this->username, 
    		$this->password, 
    		$this->dbname
    	);

		if($this->conn->connect_error) {
			die("Connection failed:" . $this->conn->connect_error);
		}
    }
}
