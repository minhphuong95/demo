<?php

require_once("Database.php");
require_once('../config/folder_image.php');

class Image extends Database
{
	public $id;
	public $name;
	public $path_name;
	public $mime_type;
	public $size;
	public $created_at;
	public $image;

	public function all()
	{
		$images = $this->getMany("SELECT * FROM images");
		$res = [];
		foreach ($images as $image) {
			$res[] = (new Image())->transform($image);
		}
		return $res;
	}

	public function find($id)
	{
		$image = $this->getOne("SELECT * FROM images WHERE id ='$id'");
		return $this->transform($image);
	}

	public function transform($image)
	{
		$this->id = $image["id"];
		$this->name = $image["name"];
		$this->path_name = $image["path_name"];
		$this->mime_type = $image["mime_type"];
		$this->size = $image["size"];
		$this->created_at = $image["created_at"];
		$this->image = FOLDER_IMAGE.$image["path_name"]."/".$image["name"];
		return $this;
	}
}