<?php

require_once("Database.php");
require_once('../config/folder_image.php');
require_once("Image.php");

class User extends Database
{
	public $id;
	public $avatar_id;
	public $name;
	public $email;
	public $password;
	public $created_at;
	public $full_name;
	public $avatar;
	public $where;

	public function all()
	{
	    $users = $this->getMany("SELECT * FROM users");
	    $res = [];
	    foreach($users as $key => $user) {
	    	$res[] = (new User())->transform($user);	 	
	    }
	    return $res;
	}

	public function where($datas)
	{
		foreach($datas as $key => $data) {
			if($key > 0) {
				$this->where .= " AND {$data[0]} {$data[1]} '{$data[2]}'";
			} else {
				$this->where .= " {$data[0]} {$data[1]} '{$data[2]}'";
			}			
		}
		return $this;	
	}

	public function get()
	{
		$users = $this->getMany("SELECT * FROM users WHERE ". $this->where);
	    $res = [];
	    foreach($users as $key => $user) {
	    	$res[] = (new User())->transform($user);	 	
	    }
	    return $res;
	}

	public function limit($limit)
	{
	    $users = $this->getMany("SELECT * FROM users LIMIT ".$limit);
	    $res = [];
	    foreach($users as $key => $user) {
	    	$res[] = (new User())->transform($user);	 	
	    }
	    return $res;
	}

	public function find($id)
	{
	    $user = $this->getOne("SELECT * FROM users WHERE id=".$id);
	    return $this->transform($user);
	}

	public function changeName()
	{
	    $this->full_name = $this->name . 'ppppppppppppppppppp'. $this->id;
	}

	public function getAvatar($avatar_id)
	{
		return $this->getOne("SELECT * FROM images WHERE id='$avatar_id'");
	}

	private function transform($user)
	{
		$this->id = $user['id'];
		$this->avatar_id = $user['avatar_id'];
		$this->name = $user['name'];
		$this->email = $user['email'];
		$this->password = $user['password'];
		$this->created_at = $user["created_at"];
		$image = new Image();
		$image->find($user['avatar_id']);
		$this->avatar = $image->image;
	    return $this;
	}
} 

