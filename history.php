<?php
session_start();
if(!isset($_SESSION['success_login'])) {
	header('Location: http://demo.local/users/login.php');
	die();
}
$email_login = null;
if(isset($_SESSION['email_login'])) {
	$email_login = $_SESSION['email_login'];
}
require_once('config/database.php');

$data = "SELECT * FROM history";
if($email_login != 'admin@admin.com') {
	$data .= " WHERE email = '$email_login'";
}
$result = $conn->query($data);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>History</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	
	<div class="container">
		<p style="color: red">
			<?php
			// if(isset($_SESSION['success_edit_user'])) {
			// 	echo $_SESSION['success_edit_user'];
			// 	unset($_SESSION['success_edit_user']);
			// }
			// if(isset($_SESSION['success_add_user'])) {
			// 	echo $_SESSION['success_add_user'];
			// 	unset($_SESSION['success_add_user']);
			// }
			// if(isset($_SESSION['success_delete_user'])) {
			// 	echo $_SESSION['success_delete_user'];
			// 	unset($_SESSION['success_delete_user']);
			// }
			// if(isset($_SESSION['success_update_password'])) {
			// 	echo $_SESSION['success_update_password'];
			// 	unset($_SESSION['success_update_password']);
			// }
			?>
		</p>
		<div>
			<a class="btn btn-primary" href="/users/add_user.php">Thêm user</a>
		</div>
		<table class="table table-bordered table-hover">
			<tr>
				<th>id</th>
				<th>email</th>
				<th>content</th>
				<th>created_at</th>
			</tr>
			
			<?php while($row = $result->fetch_assoc()) { ?>
			<tr>
	            <td><?php echo $row['id']; ?></td>
		        <td><?php echo $row['email']; ?></td>
		        <td><?php echo $row['content'] ?></a></td>
		        <td><?php echo $row['created_at']; ?></td>
		    </tr>
			<?php } ?>

		</table>
		<div>
			<a class="btn btn-primary" href="/users/handle_users/handle_logout_user.php">Đăng xuất</a>
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>
	

</body>
</html>