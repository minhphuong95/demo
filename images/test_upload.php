<?php
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
if($_SERVER['REQUEST_METHOD'] != 'POST') {
	die();
}
require_once('../config/database.php');
require_once('../path.php');
if(isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
	$date = date('d-m-Y');
	$year = date('Y', strtotime($date));
	$month = date('m', strtotime($date));
	$uniqid = uniqid();
	$name_file = pathinfo($_FILES['file']['name']);
	$tail_file = $name_file['extension'];
	$name = $uniqid.".".$tail_file;
	$path_name = "/".$year."/".$month;
	$mime_type = $_FILES['file']['type'];
	$size = ((int)$_FILES['file']['size']/1024)."KB";
	$folder_file = PATH_IMAGE."/".$year."/".$month;
	if(is_dir($folder_file) == false) {
		$folder = mkdir($folder_file, 0700, true);
	}
	move_uploaded_file($_FILES['file']['tmp_name'], $folder_file."/".$name);
	$check = "INSERT INTO images(name, path_name, mime_type, size, status) VALUES ('$name', '$path_name', '$mime_type', '$size', '1')";
	if($conn->query($check) === TRUE) {
		$_SESSION['success_add_image'] = 'Đã thêm thành công';
		header('Location: http://demo.local/images/add.php');
	} else {
		$_SESSION['error_add_image'] = 'Lỗi:'. $conn->error;
		header('Location: http://demo.local/images/add.php');
	}
} else {
	$_SESSION['error_add_image'] = 'Lỗi';
	header('Location: http://demo.local/images/add.php');
}
?>