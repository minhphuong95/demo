<?php
$page = $_GET["page"];
$limit = 5;
require_once('../config/database.php');
$sql_all = "SELECT count(*) as total FROM images";
$result_all = $conn->query($sql_all);
$data_all = $result_all->fetch_assoc();
$sum_rows = $data_all["total"];
$page_max = ceil($sum_rows/$limit);
if($page < 1) {
	$page = 1;
}
if($page > $page_max) {
	$page = $page_max;
}
$offset = $limit * ( $page - 1 );
$sql = "SELECT * FROM images LIMIT " . $limit . " OFFSET " . $offset;
$images = [];
$data = [];
if($result = $conn->query($sql)) {
	while($row = $result->fetch_assoc()) {		
		array_push($images, $row);
	}
}
$data["page"] = $page;
$data["images"] = $images;
echo json_encode($data);
?>