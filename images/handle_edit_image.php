<?php
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die();
}
$name_image = null;
if(isset($_POST["name_image"])) {
    $name_image = $_POST["name_image"];
}
$name = null;
if(isset($_POST["name"])) {
    $name = $_POST["name"];
}

$id_image = null;
if(isset($_POST['id_image'])) {
	$id_image = $_POST['id_image'];
}
$path_image = null;
if(isset($_POST['path_image'])) {
    $path_image = $_POST['path_image'];
}

require_once('../config/database.php');
$check = "SELECT * FROM images WHERE id = '$id_image'";
$result = $conn->query($check);

if($result->num_rows > 0) {
    require_once('../path.php');
	rename(PATH_IMAGE.$path_image."/".$name, PATH_IMAGE.$path_image."/".$name_image);
	$update = "UPDATE images SET name = '$name_image' WHERE id = '$id_image'";

	if($conn->query($update) === TRUE) {
		$_SESSION['success_edit_image'] = "Cập nhật thành công";
		header('Location: http://demo.local/images/images.php');
				
	} else {
		$_SESSION['error_edit_image'] = "Lỗi: " . $conn->error;
		header('Location: http://demo.local/images/edit_image.php?id=' . $id_image);
	}
} else {
	$_SESSION['error_edit_image'] = "Cập nhật không thành công";
	header('Location: http://demo.local/images/edit_image.php?id=' . $id_image);
}

?>