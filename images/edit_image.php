<?php
session_start();
$id = $_GET['id'];
require_once('../config/database.php');
require_once('../config/folder_image.php');
$type = null;
$size = null;
$name_image = null;
$path_name = null;
$status = null;
$width = null;
$height = null;

$getimages = "SELECT * FROM images WHERE id = '$id'";
$check_data = $conn->query($getimages);
if($check_data->num_rows > 0) {
	$data = mysqli_fetch_object($check_data);
	if(array_key_exists('name', $data)) {
		$name_image = $data->name;
	}
	if(array_key_exists('path_name', $data)) {
		$path_name = $data->path_name;
	}
	if(array_key_exists('mime_type', $data)) {
		$type = $data->mime_type;
	}
	if(array_key_exists('size', $data)) {
		$size = $data->size;
	}
	if(array_key_exists('width', $data)) {
		$width = $data->width;
	}
	if(array_key_exists('height', $data)) {
		$height = $data->height;
	}
	if(array_key_exists('status', $data)) {
		$status = $data->status;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Chỉnh sửa image</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	<div class="container">
		<div class="col-xs 12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form">	
			<div>
				<h3>Chỉnh sửa image</h3>
			</div>
			<div>
				<p style="color: red">
					
				</p>
			</div>
			<div>
				<form action="/images/handle_edit_image.php" method="post">
					<div class="form-group">
						<img width="100" height="100" src="<?php echo FOLDER_IMAGE.$path_name."/".$name_image ?>">
					</div>
					<div class="form-group">
						<label>Name</label>
						<div class="input-group">
							<input type="text" name="name_image" value="<?php echo $name_image; ?>">
						</div>
					</div>
					<div>
						<input type="hidden" name="id_image" value="<?php echo $id ?>">
					</div>
					<div>
						<input type="hidden" name="name" value="<?php echo $name_image ?>">
					</div>
					
					<div class="form-group">
						<label>Path</label>
						<div class="input-group">
							<input type="text" name="path_image" value="<?php echo $path_name ?>" readonly>
						</div>
						<label>Type</label>
						<div class="input-group">
							<input type="text" name="type_image" value="<?php echo $type; ?>" readonly>
						</div>
						<label>Size</label>
						<div class="input-group">
							<input type="text" name="size_image" value="<?php echo $size; ?>" readonly>
						</div>
					</div>
					
					<div class="text-center">
						<button type="submit" name="submit" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete</button>
					</div>
				</form>
				
			</div>	
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>
	
</body>
</html>