<?php
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	var_dump($_FILES['file']);
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thêm image</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
	<link rel="stylesheet" type="text/css" href="/css/dropzone.css">
</head>
<body>
	<div class="container">
		<div class="col-xs 12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form">	
			<div>
				<h3>Thêm image</h3>
			</div>
			<div>
				<p style="color: red">
				<?php
				if(isset($_SESSION['success_add_image'])) {
					echo $_SESSION['success_add_image'];
					unset($_SESSION['success_add_image']);
				}
				?>
				</p>
			</div>
			<div>
				<form action="#" method="post" id="my-awesome-dropzone" enctype="multipart/form-data">
					<div class="form-group dropzone" id="myId">
						
					</div>
					<p hidden class="show_name"></p>
					<p hidden class="show_type"></p>
					<p hidden class="show_size"></p>
				</form>

				
			</div>
			
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="/jquery/dropzone.js"></script>
	<script type="text/javascript">
		Dropzone.autoDiscover = false;
		
		$("div#myId").dropzone({ 
			url: "/images/test_upload.php",
			init: function() {
			    this.on("addedfile", function(file) { 
			    	$(".show_name").text("Name: " + file.name).show();
			    	$(".show_type").text("Type: " + file.type).show();
			    	$(".show_size").text("Size: " + file.size).show();
			    	console.log(file);
			    });
			}
		});

	</script>
</body>
</html>