<?php
require_once('../config/app.php');
require_once('../config/folder_image.php');
session_start();
if(!isset($_SESSION['success_login'])) {
	header('Location: http://demo.local/users/login.php');
	die();
}
$email_login = null;
if(isset($_SESSION['email_login'])) {
	$email_login = $_SESSION['email_login'];
}
$data_image = "SELECT * FROM images";

$result = $conn->query($data_image);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	
	<div class="container">
		<p style="color: red">
			<?php
			if(isset($_SESSION['success_edit_image'])) {
				echo $_SESSION['success_edit_image'];
				unset($_SESSION['success_edit_image']);
			}
			?>
		</p>
		<div>
			<a class="btn btn-primary" href="/index.php">Quay về trang user</a>
			<a class="btn btn-primary" href="/images/add.php">Thêm image</a>
		</div>
		<div>
			
			<?php while($row = $result->fetch_assoc()) { 
			if($row['status'] == 1) { ?>
				<a href="/images/edit_image.php?id=<?php echo $row['id'] ?>">
					<img width="100" height="100" src="<?php echo FOLDER_IMAGE.$row['path_name']."/".$row['name'] ?>" onerror = "this.src ='/asset/images/image-default.png';">
				</a>
			<?php }
			} ?>

		</div>
		<div>
			<a class="btn btn-primary" href="/users/handle_users/handle_logout_user.php">Đăng xuất</a>
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>

</body>
</html>