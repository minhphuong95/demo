<?php
session_start();
?>
<!DOCTYPE html>
<html lang="vi">
<head><
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
</head>
<body>
	<div class="container">
		<div class="col-xs 12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form">	
			<div>
				<h3>Đăng nhập</h3>
			</div>
			<div>
				<p style="color: red">
					<?php
					if(isset($_SESSION['error_login'])) {
						echo $_SESSION['error_login'];
						unset($_SESSION['error_login']);
					}
					?>
				</p>
			</div>
			<div>
				<form action="/users/handle_users/handle_login.php" method="post">
					<div class="form-group">
						<label>Email address</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-envelope"></i></span>
							<input type="email" name="email" class="form-control" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<label>Password</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-lock"></i></span>
							<input type="password" name="password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="col-xs-12 col">
						<div class="col-xs-6 checkbox col">
							<label>
								<input type="checkbox"> Remember me
							</label>
						</div>
						<div class="col-xs-6 col">
							<p class="text-right">Forgot Password?</p>
						</div>	
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-default button">Sign In</button>
					</div>
					<div class="div">
						<p>Bạn chưa có tài khoản. Nhấn vào đây <a href="/register.php">đăng ký</a></p>
					</div>
				</form>
			</div>	
		</div>
	</div>
	<script type="text/javascript" src="/jquery/jquery.js"></script>
	<script type="text/javascript" src="/bootstrap-test/js/bootstrap.js"></script>
</body>
</html>