<?php
session_start();
if(!isset($_SESSION['success_login'])) {
	header('Location: http://demo.local/users/login.php');
	die();
}
$id = $_GET['id'];
require_once('../config/database.php');
require_once('../config/folder_image.php');
$value_name = null;
$value_avatar = null;
$name_image = null;
$path_name = null;
$status = null;
$getdata = "SELECT * FROM users WHERE id = '$id'";
$result = $conn->query($getdata);
if($result->num_rows > 0) {
	$row = mysqli_fetch_object($result);
	if(array_key_exists('name', $row)) {
		$value_name = $row->name;
	}
	if(array_key_exists('avatar', $row)) {
		$value_avatar = $row->avatar;
	}
	
} else {
	echo 'Không có dữ liệu';
	die('dwkahkhatp');
}
$getimage = "SELECT * FROM images WHERE id = '$value_avatar'";
$check_data = $conn->query($getimage);
if($check_data->num_rows > 0) {
	$data = mysqli_fetch_object($check_data);
	if(array_key_exists('name', $data)) {
		$name_image = $data->name;
	}
	if(array_key_exists('path_name', $data)) {
		$path_name = $data->path_name;
	}
	if(array_key_exists('status', $data)) {
		$status = $data->status;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Chỉnh sửa user</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	<div class="container">
		<div class="col-xs 12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form">	
			<div>
				<h3>Chỉnh sửa users</h3>
			</div>
			<div>
				<p style="color: red">
					<?php
					if(isset($_SESSION['error_edit_user'])) {
						echo $_SESSION['error_edit_user'];
						unset($_SESSION['error_edit_user']);
					}
					if(isset($_SESSION['error_delete_user'])) {
						echo $_SESSION['error_delete_user'];
						unset($_SESSION['error_delete_user']);
					}
					?>
				</p>
			</div>
			<div>
				<form action="/users/handle_users/handle_edit_users.php?id=<?php echo $id ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label>Họ và tên</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-user"></i></span>
							<input type="text" name="name" class="form-control" value="<?php echo $value_name; ?>">
						</div>
					</div>
					<?php if($path_name != null && $status == 1) { ?>
					<div class="form-group">
						<img width="100" height="100" class="hide_avatar change_image" src="<?php echo FOLDER_IMAGE.$path_name."/".$name_image;?>">
						<button type="button" class="btn btn-danger clear">Xóa ảnh</button>
						<button type="button" class="btn btn-primary btn-image" selection="selection_image" data-toggle="modal" data-target="#imageModal">Chọn ảnh</button>
						<input type="hidden" class="input_avatar" name="avatar" value="1">
						<input type="hidden" class="change_id" name="id_image" value="<?php echo $value_avatar; ?>">
					</div>
					<?php } ?>
					<div class="form-group">
					    <label>File input</label>
					    <input type="file" name="file">
					</div>
					<div class="text-center">
						<button type="submit" name="submit" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete</button>
					</div>
				</form>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" ria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title">Thông báo</h4>
				      </div>
				      <div class="modal-body">
				        <p>Bạn có muốn chắc chắn xóa không</p>
				      </div>
				      <form action="/users/handle_users/handle_delete_users.php" method="post">
					    <div class="modal-footer">
					    	<input type="hidden" name="id" value="<?php echo $id ?>">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-danger">Delete</button>
					    </div>
				      </form>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" ria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				      </div>
				      <div class="modal-body" id="show-image">
				      </div>
				      <div class="text-center">
					      <button type="button" class="btn btn-primary pre" pre="pre">pre</button>
					      <button type="button" class="btn btn-primary next" next="next">next</button>
				      </div>
				      <form action="/users/handle_users/handle_delete_users.php" method="post">
					    <div class="modal-footer">
					    	<input type="hidden" name="id" value="<?php echo $id ?>">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-danger">Delete</button>
					    </div>
				      </form>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div>	
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>
	<script type="text/javascript">
		var page = 1;
		function exportImage(value) {	
			$.ajax({
			    method: "GET",
			    url: "/images/ajax_image.php",
			    data: {page : value}
			})
			.done(function( msg ) {
			  	var data = JSON.parse(msg);
		  		var content = "";
  		 		data["images"].forEach(function(item, key){
  		 			content += "<img width='100' height='100' class='selection-image' id_image='"+ item.id+"' src='/upload"+ item.path_name +"/"+item.name +"'>"			
	  		 	});
	  		 	console.log(content);
	  		 	$('#show-image').html(content);
	  		 	page = parseInt(data["page"]);
			});
		}
		$(document).ready(function(){
			$(".clear").click(function(){
				$(this).parent().hide();
				$(this).parent().find(".input_avatar").val('0');
			});
			$(".btn-image").click(function(){
				exportImage(1);
			});
			$(".next").click(function(){
				page = page + 1;			
				exportImage(page);
			});
			$(".pre").click(function(){
				page = page - 1;			
				exportImage(page);
			});
			$(document).on("click",".selection-image",function(){
				var src = $(this).attr("src");
				var id = $(this).attr("id_image");
				$(".change_image").attr("src", src);
				$(".change_id").val(id);
			});
		})
	</script>
</body>
</html>