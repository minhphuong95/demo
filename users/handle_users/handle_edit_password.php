<?php
session_start();
$current_password = null;
$new_password = null;
$id = null;
$new_pass_hash = null;
$email_login = null;
if(isset($_SESSION['email_login'])) {
	$email_login = $_SESSION['email_login'];
}
if(isset($_POST["current_password"])) {
	$current_password = $_POST["current_password"];
}
if(isset($_POST["new_password"])) {
	$new_password = $_POST["new_password"];
	$new_pass_hash = password_hash($_POST["new_password"], PASSWORD_DEFAULT);
}
if(isset($_POST["id"])) {
	$id = $_POST["id"];
}
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	die();
}

require_once('../../config/database.php');

$check = "SELECT password FROM users WHERE id = '$id'";
$result = $conn->query($check);

if($result->num_rows > 0) {
	$row = mysqli_fetch_object($result);
	$verify = password_verify($current_password, $row->password);
	if($verify) {
		$update = "UPDATE users SET password = '$new_pass_hash' WHERE id = '$id'";
		if($conn->query($update) === TRUE) {
			$_SESSION['success_update_password'] = "Cập nhật mật khẩu thành công";
			header('Location: http://demo.local/index.php');
			$content = $email_login . ' ' . 'đã thay đổi password với id =' . ' ' . $id;
		    $history = "INSERT INTO history (email, content) VALUES ('$email_login', '$content')";
		    if(mysqli_query($conn, $history)) {
		    	$_SESSION['success_history_password'] = 'Lưu thành công';
		    }	
		} else {
			$_SESSION['error_update_password'] = "Lỗi: " . $conn->error;
			header('Location: http://demo.local/users/edit_password.php?id=' . $id);
		}
	} else {
		$_SESSION['error_update_password'] = "Mật khẩu hiện tại không đúng";
		header('Location: http://demo.local/users/edit_password.php?id=' . $id);
	}
	
} else {
	echo 'Không có dữ liệu';
	die();
}
?>