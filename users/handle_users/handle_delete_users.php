<?php
session_start();
$id = null;
if(isset($_POST["id"])) {
	$id = $_POST["id"];
}
$email_login = null;
if(isset($_SESSION['email_login'])) {
	$email_login = $_SESSION['email_login'];
}
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	die();
}
require_once('../../config/database.php');
$check = "SELECT email FROM users WHERE id = '$id'";
$result = $conn->query($check);

if($result->num_rows > 0) {
	$delete = "DELETE FROM users WHERE id = '$id'";
	if($conn->query($delete) === TRUE) {
		$_SESSION['success_delete_user'] = "Xóa thành công";
		header('Location: http://demo.local/index.php');
		$content = $email_login . ' ' . 'đã xóa user với id =' . ' ' . $id;
	    $history = "INSERT INTO history (email, content) VALUES ('$email_login', '$content')";
	    if(mysqli_query($conn, $history)) {
	    	$_SESSION['success_history_delete'] = 'Xóa thành công';
	    }	
	} else {
		$_SESSION['error_delete_user'] = "Lỗi: " . $conn->error;
		header('Location: http://demo.local/users/edit_users.php?id=' . $id);
	}
} else {
	$_SESSION['error_delete_user'] = "Xóa không thành công";
	header('Location: http://demo.local/users/edit_users.php?id=' . $id);
}
?>