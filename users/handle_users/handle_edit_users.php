<?php
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
$id = $_GET['id'];
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die();
}
$email_login = null;
if(isset($_SESSION['email_login'])) {
	$email_login = $_SESSION['email_login'];
}
$name = $_POST["name"];
$avatar = null;
if(isset($_POST["avatar"])) {
	$avatar = $_POST["avatar"];
}
$id_image = null;
if(isset($_POST['id_image'])) {
	$id_image = $_POST['id_image'];
}

require_once('../../config/database.php');
$check = "SELECT email, avatar FROM users WHERE id = '$id'";
$result = $conn->query($check);

if($result->num_rows > 0) {
	require_once('../../path.php');
	$avatar_id = null;
	$set = "UPDATE users SET name = '$name'";
	$where = " WHERE id = '$id'";
	$insert = null;
	if(isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
		$date = date('d-m-Y H:i:s');
    	$year = date('Y', strtotime($date));
    	$month = date('m', strtotime($date));
    	$uniqid = uniqid();
    	$name_file = pathinfo($_FILES['file']['name']);
    	$tail_file = $name_file['extension'];
    	$name_image = $uniqid . "." . $tail_file;
    	$path_file = "/".$year."/".$month;
    	$year_folder = PATH_IMAGE."/".$year;
    	$month_folder = $year_folder."/".$month;

    	if(is_dir($month_folder) == false) {
    		$folder = mkdir($month_folder, 0700, true);
    	}
        move_uploaded_file($_FILES['file']['tmp_name'], $month_folder."/".$name_image);
        $mime_type = $_FILES['file']['type'];
        $size = ((int)$_FILES['file']['size']/1024)."KB";
        $insert = "INSERT INTO images (name, path_name, mime_type, size, status) VALUES ('$name_image', '$path_file', '$mime_type', '$size', '1')";
        if($id_image != null) {
        	$insert = "UPDATE images SET name = '$name_image', path_name = '$path_file', mime_type = '$mime_type', size = '$size' WHERE id = '$id_image'";
        }
        if($conn->query($insert) === TRUE) {
        	$_SESSION['success_image_user'] = "Cập nhật thành công";
        	if($id_image != null) {
        		$avatar_id = $id_image;
        	} else {
        		$avatar_id = mysqli_insert_id($conn);
        	}
        } 
	} else {
    	if(!$avatar) {
    		$insert = "UPDATE images SET status ='-1' WHERE id = '$id_image'";
    		if($conn->query($insert) === TRUE) {
    			$_SESSION['success_image_user'] = "Cập nhật thành công";
    		}
    	}
    	$avatar_id = $id_image;
    }
	$update = $set . ", avatar = '$avatar_id'" . $where;
	if($conn->query($update) === TRUE) {
		$_SESSION['success_edit_user'] = "Cập nhật thành công";
		header('Location: http://demo.local/index.php');
		$content = $email_login . ' ' . 'đã thay đổi name với id =' . ' ' . $id;
	    $history = "INSERT INTO history (email, content) VALUES ('$email_login', '$content')";
	    if(mysqli_query($conn, $history)) {
	    	$_SESSION['success_history_name'] = 'Lưu thành công';
	    }		
	} else {
		$_SESSION['error_edit_user'] = "Lỗi: " . $conn->error;
		header('Location: http://demo.local/users/edit_users.php?id=' . $id);
	}
} else {
	$_SESSION['error_edit_user'] = "Cập nhật không thành công";
	header('Location: http://demo.local/users/edit_users.php?id=' . $id);
}

?>