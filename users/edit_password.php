<?php
session_start();
$id = $_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thay đổi mật khẩu</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	<div class="container">
		<div class="col-xs 12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form">	
			<div>
				<h3>Thay đổi mật khẩu</h3>
			</div>	
			<div>
				<p style="color: red">
					<?php
					if(isset($_SESSION['error_update_password'])) {
						echo $_SESSION['error_update_password'];
						unset($_SESSION['error_update_password']);
					}
					?>
				</p>
			</div>
			<div>
				<form action="/users/handle_users/handle_edit_password.php" method="post">
					<div class="form-group">
						<label>Mật khẩu hiện tại</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-lock"></i></span>
							<input type="password" name="current_password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<label>Mật khẩu mới</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-lock"></i></span>
							<input type="password" name="new_password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="input-group">
						<input type="hidden" name="id" class="form-control" value="<?php echo $id ?>">
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-primary button">Save</button>
					</div>
				</form>
			</div>	
		</div>
	</div>
	<script src="/jquery/jquery.js"></script>
	<script src="/bootstrap-test/js/bootstrap.js"></script>
</body>
</html>