<?php
session_start();
?>
<!DOCTYPE html>
<html lang="vi">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="/bootstrap-test/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="/demo.css">
</head>
<body>
	
	<div class="container">
		<div class="col-xs 12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 form">	
			<div>
				<h3>Đăng ký</h3>
			</div>
			<div>
				<p style="color: red">
					<?php
					if(isset($_SESSION['success'])) {
						echo $_SESSION['success'];
						session_destroy();
					}
					if(isset($_SESSION['error'])) {
						echo $_SESSION['error'];
						session_destroy();
					}
					?>
				</p>
			</div>
			<div>
				<form action="/users/handle_users/handle_register.php" method="post">
					<div class="form-group">
						<label>Họ và tên</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-user"></i></span>
							<input type="text" name="name" nameinput="name" content_required="is required" max="15" min="5" class="form-control check_max_min check_required" content_max="độ dài ký tự tối đa là 15" content_min="độ dài ký tự tối thiểu là 5" placeholder="Name">
							<span class="display show_content"></span>
						</div>
					</div>
					<div class="form-group">
						<label>Email</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-envelope"></i></span>
							<input type="text" name="email" nameinput="email" content_required="is required" class="form-control check_required" placeholder="Email">
							<span class="display show_content"></span>
						</div>
					</div>
					<div class="form-group">
						<label>Password</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fas fa-lock"></i></span>
							<input type="text" name="password" nameinput="password" content_required="is required" class="form-control check_required" placeholder="Email">
							<span class="display show_content"></span>
						</div>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-default button">Sign Up</button>
					</div>
					<div class="div">
						<p>Bạn đã có tài khoản. Nhấn vào đây <a href="/login.php">đăng nhập</a></p>
					</div>
				</form>
			</div>	
		</div>
	</div>
	<script type="text/javascript" src="/jquery/jquery.js"></script>
	<script type="text/javascript" src="/bootstrap-test/js/bootstrap.js"></script>
	<script type="text/javascript">
		var check = {};

		function check_required(check_element) {
			var show_content = check_element.parent().find(".show_content");
			var name_input = check_element.attr("nameinput");
			var name = check_element.attr("name");
			var length = check_element.val().trim().length;
			if(length == 0){
				var content_required = check_element.attr("content_required");
				show_content.text(name_input + " " + content_required).show();
				check[name] = false;
			} else {
				if(!check_element.hasClass("check_max_min")) {
					show_content.hide();
					check[name] = true;
				}	
			}	
		}

		function check_max_min(check_element) {
			var length = check_element.val().trim().length;				
			var name_input = check_element.attr("nameinput");
			var name = check_element.attr("name");
			var min = check_element.attr("min");
			var max = check_element.attr("max");
			var show_content = check_element.parent().find(".show_content");
			if(length < parseInt(min) || length > parseInt(max)) {
				console.log('max')
				var content;
				if(length < parseInt(min)) {
					content = check_element.attr("content_min");
				} 
				if(length > parseInt(max)) {
					content = check_element.attr("content_max");
				}
				show_content.text(name_input + " " + content).show();
				check[name] = false;
			} else {
				show_content.hide();
				check[name] = true;
			}
		}

		$(document).ready(function(){		
			$(document).on('input', '.check_max_min', function(){
				var check_max_min_input = $(this);
				check_max_min(check_max_min_input);
			});

			$(document).on('input', '.check_required', function(){
				var check_required_input = $(this);
				check_required(check_required_input);	
			});
				
			$('form').submit(function(event){	
				event.preventDefault();

				if($("input").hasClass("check_max_min")) {
					$(".check_max_min").each(function(){
						var check_max_min_submit = $(this);
						check_max_min(check_max_min_submit);
					});	
				}

				if($("input").hasClass("check_required")) {
					$(".check_required").each(function(){
						var check_required_submit = $(this);
						check_required(check_required_submit);
					});
				}

				var text = true;
				for(key in check) {
					if(!check[key]) {
						text = false;
					}					
				}
				if(text) {
					 event.currentTarget.submit();
				} else {
					console.log("không mở form submit");
				}	
				
			});
		});

	</script>
</body>
</html>